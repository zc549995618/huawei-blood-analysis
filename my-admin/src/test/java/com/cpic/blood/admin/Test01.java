package com.cpic.blood.admin;

import cn.hutool.core.lang.Validator;

public class Test01 {

    public static void main(String[] args) {
        double value1 = 10.5;
        double value2 = 0.0;

        boolean isValid1 = Validator.isNotNull(value1);
        boolean isValid2 = Validator.isNotNull(value2);
        boolean isValid3 = Validator.isNotNull(null);

        System.out.println("Value 1 is not null: " + isValid1);
        System.out.println("Value 2 is not null: " + isValid2);
        System.out.println("Value 3 is not null: " + isValid3);
    }
}
