package com.cpic.blood.admin;

import cn.hutool.core.util.IdUtil;
import com.cpic.blood.admin.service.impl.FileUploadService;
import com.cpic.blood.core.domain.TCafUser;
import com.cpic.blood.core.domain.Users;
import com.cpic.blood.core.mapper.TCafUserMapper;
import com.cpic.blood.core.mapper.UsersMapper;
import com.cpic.blood.core.service.TCafUserService;
import com.cpic.blood.core.utils.ConfigUtil;
import com.cpic.blood.core.utils.FtpFileUtil;
import com.cpic.blood.core.utils.FtpUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Async;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@SpringBootTest
@Slf4j
public class SpringBootTests {

    @Autowired
    FileUploadService fileUploadService;

    @Resource
    TCafUserMapper tCafUserMapper;

    @Resource
    TCafUserService tCafUserService;

    @Resource
    UsersMapper usersMapper;

    @Value("${ftp.host}")
    public String ftpHost;

    @Value("${ftp.port}")
    public String ftpPort;

    @Value("${ftp.username}")
    public String ftpUserName;

    @Value("${ftp.password}")
    public String ftpPassWord;

    // 创建索引
    @Test
    public void testCreate() throws Exception {
        System.out.println(ConfigUtil.CONFIG_PATH);
        System.out.println(ConfigUtil.HIVE_INPUT_DIR);
    }

    @Test
    @Async
    public void sayHello() {
        log.info("start say hello");
        System.out.println(Thread.currentThread().getName());
        System.out.println("hello");
        log.info("end say hello");
    }

    @Test
    public void testFtp() {
        FtpFileUtil.connectTest(ftpHost,Integer.valueOf(ftpPort),ftpUserName,ftpPassWord);
    }

    @Test
    public void testFtpUtil() throws Exception {
        FtpUtil.getConnect(ftpHost,Integer.valueOf(ftpPort),ftpUserName,ftpPassWord);
        FtpUtil.close();
    }

    @Test
    public void testOss() throws IOException {
        List<TCafUser> list = tCafUserService.list();
        for (TCafUser tCafUser : list) {
            System.out.println(tCafUser);
        }
    }

    @Test
    public void testUUID() {
        System.out.println(IdUtil.fastSimpleUUID());
    }

    @Test
    public void testUsersMapper(){
        Users users = usersMapper.findByUsername("zhangsan");
        System.out.println(users);
    }
}
