package com.cpic.blood.admin;

import com.alibaba.fastjson.JSONArray;
import com.cpic.blood.core.utils.ConfigUtil;
import com.cpic.blood.core.utils.DateUtil;
import com.cpic.blood.core.vo.HiveFileConfigVo;
import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Dom4jParseUserXmlTest {

    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void main(String[] args) {
        //创建解析器对象
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(Dom4jParseUserXmlTest.class.getClassLoader().getResource("job_conf.xml"));
            Element rootElement = document.getRootElement();
            HiveFileConfigVo vo = null;
            if (rootElement != null) {
                vo = new HiveFileConfigVo();
            }
            System.out.println("1.------->job_conf.xml文件的根节点的名字是:" + rootElement.getName());

            System.out.println("2.------->获取根标签properties的子标签列表");
            List<Element> usersSubElementList = rootElement.elements();
            for (Element userElement : usersSubElementList) {
                List<Element> userSubElementList = userElement.elements();
                for (Element userSubElement : userSubElementList) {
                    if (userSubElement.getName().startsWith("name")) {
                        if (ConfigUtil.HIVE_SQL.equals(userSubElement.getText())) {
                            System.out.println("SQL代码的子标签名为:" + userSubElement.getName());
                            System.out.println("SQL代码的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_EXECUTION_TIME.equals(userSubElement.getText())) {
                            System.out.println("执行时间的子标签名为:" + userSubElement.getName());
                            System.out.println("执行时间的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_SQL_TYPE.equals(userSubElement.getText())) {
                            System.out.println("SQL类型的子标签名为:" + userSubElement.getName());
                            System.out.println("SQL类型的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_PROJECT_SPACE.equals(userSubElement.getText())) {
                            System.out.println("项目空间的子标签名为:" + userSubElement.getName());
                            System.out.println("项目空间的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_SUBMITTER.equals(userSubElement.getText())) {
                            System.out.println("提交人的子标签名为:" + userSubElement.getName());
                            System.out.println("提交人的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        }
                    }
                    String name = threadLocal.get();
                    if (name != null) {
                        if (userSubElement.getName().startsWith("value")) {
                            vo.setHiveSqlType(ConfigUtil.HIVE_SQL_TYPE);
                            if (ConfigUtil.HIVE_SQL.equals(name)) {
                                System.out.println("SQL代码的子标签名为:" + userSubElement.getName());
                                System.out.println("SQL代码的子标签文本是:" + userSubElement.getText());
                                vo.setHiveSql(userSubElement.getText());
                            } else if (ConfigUtil.HIVE_EXECUTION_TIME.equals(name)) {
                                System.out.println("执行时间的子标签名为:" + userSubElement.getName());
                                System.out.println("执行时间的子标签文本是:" + userSubElement.getText());
                                vo.setHiveExecutionTime(userSubElement.getText());
                            } else if (ConfigUtil.HIVE_PROJECT_SPACE.equals(name)) {
                                System.out.println("项目空间的子标签名为:" + userSubElement.getName());
                                System.out.println("项目空间的子标签文本是:" + userSubElement.getText());
                                vo.setHiveProjectSpace(userSubElement.getText());
                            } else if (ConfigUtil.HIVE_SUBMITTER.equals(name)) {
                                System.out.println("提交人的子标签名为:" + userSubElement.getName());
                                System.out.println("提交人的子标签文本是:" + userSubElement.getText());
                                vo.setHiveSubmitter(userSubElement.getText());
                            }
                            threadLocal.remove();
                        }
                    }
                }
            }
            List<HiveFileConfigVo> list = new ArrayList<>();
            list.add(vo);
            String json = JSONArray.toJSONString(list);
            JSONArray jsonArray = JSONArray.parseArray(json);
            String filePath = ConfigUtil.HIVE_OUT_DIR+"hive_output_"+ DateUtil.getSimpleDateTime()+".txt";
            FileUtils.writeLines(new File(filePath),jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            threadLocal.remove();
        }
    }
}
