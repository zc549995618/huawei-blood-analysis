package com.cpic.blood.admin.service.impl;

import com.cpic.blood.admin.service.ApiAllServie;
import com.cpic.blood.core.utils.ConfigUtil;
import com.cpic.blood.core.utils.HiveFileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ApiAllServiceImpl implements ApiAllServie {

    @Override
    @Async
    public void startHiveFileJob() throws Exception {
        HiveFileUtils.runHiveFileTask(ConfigUtil.HIVE_INPUT_DIR);
    }
}
