package com.cpic.blood.job.thread;

import cn.hutool.core.util.IdUtil;
import com.cpic.blood.core.utils.RedisLockUtils;
import com.cpic.blood.job.common.Constant;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TurnJobThread implements Runnable {

    private RedisLockUtils redisLockUtils;

    public TurnJobThread(RedisLockUtils redisLockUtils) {
        this.redisLockUtils = redisLockUtils;
    }

    @Override
    public void run() {
        boolean lock;
        String uuid = null;
        while (true){
            log.info("TurnJobThread start");
            uuid = IdUtil.fastSimpleUUID();
            try {
                lock = redisLockUtils.getLock(Constant.JOB_LOCK, uuid);
                if(!lock){
                    log.info("{} 未获取锁，休息600毫秒",uuid);
                    Thread.sleep(600);
                }
            }catch (Exception e){
                log.error("TurnJobThread error {}",e.getMessage());
            }finally {
                redisLockUtils.releaseLock(Constant.JOB_LOCK,uuid);
                log.info("{} 释放锁成功",uuid);
            }
        }
    }
}
