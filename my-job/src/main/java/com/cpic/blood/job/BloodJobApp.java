package com.cpic.blood.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.cpic.blood.core","com.cpic.blood.job"})
@Slf4j
public class BloodJobApp {

    public static void main(String[] args) {
        SpringApplication.run(BloodJobApp.class,args);
        log.info("项目启动成功...");
    }
}
