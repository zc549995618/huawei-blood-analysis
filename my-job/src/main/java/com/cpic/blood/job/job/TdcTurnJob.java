package com.cpic.blood.job.job;

import com.cpic.blood.core.thread.ThreadPoolUtils;
import com.cpic.blood.core.utils.RedisLockUtils;
import com.cpic.blood.job.thread.TurnJobThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class TdcTurnJob {

    @Autowired
    private RedisLockUtils redisLockUtils;

    @PostConstruct
    public void init(){
        ThreadPoolUtils newInstance = ThreadPoolUtils.getNewInstance();
        TurnJobThread turnJobThread = new TurnJobThread(redisLockUtils);
        newInstance.executor(turnJobThread);
    }
}
