package com.cpic.blood.core.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author chengxuzhang
 * @since 2023-05-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_caf_user")
@ApiModel(value="TCafUser对象", description="")
public class TCafUser implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_code")
    private String userCode;

    @TableField("password")
    private String password;

    @TableField("name")
    private String name;

    @TableField("tel_phone")
    private String telPhone;

    @TableField("email")
    private String email;

    @TableField("status")
    private String status;

    @TableField("remark")
    private String remark;

    @TableField("icon")
    private String icon;

    @TableField("gender")
    private Integer gender;

    @TableField("deleted")
    @TableLogic
    private Integer deleted;

    @TableField("tree_prop")
    private String treeProp;

    @ApiModelProperty(value = "是否是P13账号")
    @TableField("is_p13")
    private String isP13;

    @TableField("update_time")
    private Date updateTime;

    @TableField("group_code")
    private String groupCode;

    @TableField("calendar_code")
    private String calendarCode;

    @TableField("create_time")
    private Date createTime;

    @TableField("create_user")
    private String createUser;

    @TableField("update_user")
    private String updateUser;

    @TableField("caf_protect")
    private String cafProtect;

    @TableField("extend")
    private String extend;

    @TableField("group_count")
    private Integer groupCount;

    @TableField("last_login_time")
    private String lastLoginTime;

    @TableField("source")
    private Integer source;


}
