package com.cpic.blood.core.vo;

import java.io.Serializable;

public class HiveFileConfigVo implements Serializable {


    private String fileName;
    private String hiveSql;
    private String hiveExecutionTime;
    private String hiveSqlType;
    private String hiveProjectSpace;
    private String hiveSubmitter;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getHiveSql() {
        return hiveSql;
    }

    public void setHiveSql(String hiveSql) {
        this.hiveSql = hiveSql;
    }

    public String getHiveExecutionTime() {
        return hiveExecutionTime;
    }

    public void setHiveExecutionTime(String hiveExecutionTime) {
        this.hiveExecutionTime = hiveExecutionTime;
    }

    public String getHiveSqlType() {
        return hiveSqlType;
    }

    public void setHiveSqlType(String hiveSqlType) {
        this.hiveSqlType = hiveSqlType;
    }

    public String getHiveProjectSpace() {
        return hiveProjectSpace;
    }

    public void setHiveProjectSpace(String hiveProjectSpace) {
        this.hiveProjectSpace = hiveProjectSpace;
    }

    public String getHiveSubmitter() {
        return hiveSubmitter;
    }

    public void setHiveSubmitter(String hiveSubmitter) {
        this.hiveSubmitter = hiveSubmitter;
    }

    @Override
    public String toString() {
        return "HiveFileConfigVo{" +
                "fileName='" + fileName + '\'' +
                ", hiveSql='" + hiveSql + '\'' +
                ", hiveExecutionTime='" + hiveExecutionTime + '\'' +
                ", hiveSqlType='" + hiveSqlType + '\'' +
                ", hiveProjectSpace='" + hiveProjectSpace + '\'' +
                ", hiveSubmitter='" + hiveSubmitter + '\'' +
                '}';
    }
}
