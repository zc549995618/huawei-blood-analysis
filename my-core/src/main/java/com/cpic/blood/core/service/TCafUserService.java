package com.cpic.blood.core.service;

import com.cpic.blood.core.domain.TCafUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface TCafUserService extends IService<TCafUser> {

}
