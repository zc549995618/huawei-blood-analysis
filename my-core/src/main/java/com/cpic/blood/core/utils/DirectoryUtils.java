package com.cpic.blood.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DirectoryUtils {
    public static List<String> getFilesAndDirectories(String relativeDirectoryPath) {
        List<String> fileList = new ArrayList<>();

//        String projectDirectory = System.getProperty("user.dir");
//        String absoluteDirectoryPath = projectDirectory + File.separator + relativeDirectoryPath;

        File directory = new File(relativeDirectoryPath);
        if (directory.exists() && directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    fileList.add(file.getAbsolutePath());
                }
            }
        }

        log.info("fileList:"+fileList.size());

        return fileList;
    }

}

