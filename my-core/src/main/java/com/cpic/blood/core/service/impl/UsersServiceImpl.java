package com.cpic.blood.core.service.impl;

import com.cpic.blood.core.domain.Users;
import com.cpic.blood.core.mapper.UsersMapper;
import com.cpic.blood.core.service.UsersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangchen-017
 * @since 2023-05-31
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {

}
