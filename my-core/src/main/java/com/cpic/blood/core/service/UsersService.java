package com.cpic.blood.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cpic.blood.core.domain.Users;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangchen-017
 * @since 2023-05-31
 */
public interface UsersService extends IService<Users> {

}
