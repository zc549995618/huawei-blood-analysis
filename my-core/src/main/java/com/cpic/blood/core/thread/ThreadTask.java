package com.cpic.blood.core.thread;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * @Author ScholarTang
 * @Date 2021/11/3 下午2:14
 * @Desc 任务线程
 */

@Slf4j
public class ThreadTask implements Runnable{

    private String inputFile;
    private String outputFile;

    public ThreadTask(String inputFile, String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    @Override
    public void run() {
        log.info("thread task：" + this.toString() + "，task start！");
        try {
            FileUtils.copyFile(new File(inputFile),new File(outputFile));
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
