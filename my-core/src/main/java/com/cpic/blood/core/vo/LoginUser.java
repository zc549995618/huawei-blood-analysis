package com.cpic.blood.core.vo;

import lombok.Data;

import java.util.Date;

@Data
public class LoginUser {

    private Integer id;

    private String username;

    private String password;

    private String email;

    private Boolean enabled;

    private Date createdTime;

    private Date updatedTime;
}
