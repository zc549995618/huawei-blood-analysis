package com.cpic.blood.core.thread;

import com.cpic.blood.core.job.Dom4jParseHiveXml;
import com.cpic.blood.core.vo.HiveFileConfigVo;

import java.util.concurrent.Callable;

public class BloodCallable implements Callable {

    private String filePath;

    public BloodCallable(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public Object call() throws Exception {
        HiveFileConfigVo vo = Dom4jParseHiveXml.parseXMLFile(filePath);
        return vo;
    }
}
