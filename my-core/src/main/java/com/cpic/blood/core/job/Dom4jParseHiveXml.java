package com.cpic.blood.core.job;

import com.cpic.blood.core.utils.ConfigUtil;
import com.cpic.blood.core.vo.HiveFileConfigVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.List;

@Slf4j
public class Dom4jParseHiveXml {

    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static HiveFileConfigVo parseXMLFile(String fileName){
        int indexOf = fileName.lastIndexOf("\\");
        String file = fileName.substring(indexOf+1);
        System.out.println(file);
        File inputFile = new File(fileName);
        //创建解析器对象
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(inputFile);
            Element rootElement = document.getRootElement();
            HiveFileConfigVo vo = null;
            if (rootElement != null) {
                vo = new HiveFileConfigVo();
            }
            log.debug("1.------->job_conf.xml文件的根节点的名字是:" + rootElement.getName());

            log.debug("2.------->获取根标签properties的子标签列表");
            List<Element> usersSubElementList = rootElement.elements();
            for (Element userElement : usersSubElementList) {
                List<Element> userSubElementList = userElement.elements();
                for (Element userSubElement : userSubElementList) {
                    if (userSubElement.getName().startsWith("name")) {
                        if (ConfigUtil.HIVE_SQL.equals(userSubElement.getText())) {
                            log.debug("SQL代码的子标签名为:" + userSubElement.getName());
                            log.debug("SQL代码的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_EXECUTION_TIME.equals(userSubElement.getText())) {
                            log.debug("执行时间的子标签名为:" + userSubElement.getName());
                            log.debug("执行时间的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_SQL_TYPE.equals(userSubElement.getText())) {
                            log.debug("SQL类型的子标签名为:" + userSubElement.getName());
                            log.debug("SQL类型的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_PROJECT_SPACE.equals(userSubElement.getText())) {
                            log.debug("项目空间的子标签名为:" + userSubElement.getName());
                            log.debug("项目空间的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        } else if (ConfigUtil.HIVE_SUBMITTER.equals(userSubElement.getText())) {
                            log.debug("提交人的子标签名为:" + userSubElement.getName());
                            log.debug("提交人的子标签文本是:" + userSubElement.getText());
                            threadLocal.set(userSubElement.getText());
                        }
                    }
                    String name = threadLocal.get();
                    if (name != null) {
                        if (userSubElement.getName().startsWith("value")) {
                            vo.setFileName(file);
                            vo.setHiveSqlType(ConfigUtil.HIVE_SQL_TYPE);
                            if (ConfigUtil.HIVE_SQL.equals(name)) {
                                log.debug("SQL代码的子标签名为:" + userSubElement.getName());
                                log.debug("SQL代码的子标签文本是:" + userSubElement.getText());
                                if(StringUtils.isNotEmpty(userSubElement.getText())){
                                    String hiveSql = userSubElement.getText().replaceAll("\\n", "").trim();
                                    vo.setHiveSql(hiveSql);
                                }
                            } else if (ConfigUtil.HIVE_EXECUTION_TIME.equals(name)) {
                                log.debug("执行时间的子标签名为:" + userSubElement.getName());
                                log.debug("执行时间的子标签文本是:" + userSubElement.getText());
                                vo.setHiveExecutionTime(userSubElement.getText());
                            } else if (ConfigUtil.HIVE_PROJECT_SPACE.equals(name)) {
                                log.debug("项目空间的子标签名为:" + userSubElement.getName());
                                log.debug("项目空间的子标签文本是:" + userSubElement.getText());
                                vo.setHiveProjectSpace(userSubElement.getText());
                            } else if (ConfigUtil.HIVE_SUBMITTER.equals(name)) {
                                log.debug("提交人的子标签名为:" + userSubElement.getName());
                                log.debug("提交人的子标签文本是:" + userSubElement.getText());
                                vo.setHiveSubmitter(userSubElement.getText());
                            }
                            threadLocal.remove();
                        }
                    }
                }
            }
            return vo;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            threadLocal.remove();
        }
        return null;
    }
}
