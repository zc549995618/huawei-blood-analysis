package com.cpic.blood.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cpic.blood.core.domain.TCafUser;
import com.cpic.blood.core.service.TCafUserService;
import com.cpic.blood.core.mapper.TCafUserMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class TCafUserServiceImpl extends ServiceImpl<TCafUserMapper, TCafUser>
    implements TCafUserService{

}




