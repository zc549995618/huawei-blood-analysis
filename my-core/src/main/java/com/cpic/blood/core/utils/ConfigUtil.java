package com.cpic.blood.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

@Slf4j
public class ConfigUtil {

    public static String CONFIG_PATH =System.getProperty("user.dir")+ File.separator+"config"+File.separator+ "config.properties";

    private static Properties properties = new Properties();
    public static String HIVE_SQL;
    public static String HIVE_EXECUTION_TIME;
    public static String HIVE_SQL_TYPE;
    public static String HIVE_PROJECT_SPACE;
    public static String HIVE_SUBMITTER;

    public static String HIVE_OUT_DIR;
    public static String HIVE_INPUT_DIR;

    public ConfigUtil() {
    }

    static {
        try(InputStream in = new FileInputStream(CONFIG_PATH)){
            log.info(CONFIG_PATH);
            properties.load(in);
            HIVE_SQL = properties.getProperty("HIVE_SQL");
            HIVE_EXECUTION_TIME = properties.getProperty("HIVE_EXECUTION_TIME");
            HIVE_SQL_TYPE = properties.getProperty("HIVE_SQL_TYPE");
            HIVE_PROJECT_SPACE = properties.getProperty("HIVE_PROJECT_SPACE");
            HIVE_SUBMITTER = properties.getProperty("HIVE_SUBMITTER");
            HIVE_OUT_DIR = properties.getProperty("HIVE_OUT_DIR");
            HIVE_INPUT_DIR = properties.getProperty("HIVE_INPUT_DIR");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        log.info(CONFIG_PATH);
    }

}
