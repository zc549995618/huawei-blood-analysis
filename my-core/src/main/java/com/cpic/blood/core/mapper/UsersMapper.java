package com.cpic.blood.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cpic.blood.core.domain.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangchen-017
 * @since 2023-05-31
 */
@Mapper
public interface UsersMapper extends BaseMapper<Users> {

    Users findByUsername(@Param("username") String username);
}
