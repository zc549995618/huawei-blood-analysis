package com.cpic.blood.core.mapper;

import com.cpic.blood.core.domain.TCafUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.cpic.blood.core.domain.TCafUser
 */
public interface TCafUserMapper extends BaseMapper<TCafUser> {

}




