import java.security.SecureRandom;
import java.util.Base64;

public class UtilTest {

	public static void main(String[] args) {
		SecureRandom secureRandom = new SecureRandom();
		byte[] keyBytes = new byte[32]; // 32字节密钥，可以根据需要调整大小
		secureRandom.nextBytes(keyBytes);
		String secretKey = Base64.getEncoder().encodeToString(keyBytes);
		System.out.println(secretKey);
	}
}
