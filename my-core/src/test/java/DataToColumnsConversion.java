import com.cpic.blood.core.dto.ColumnData;

import java.util.ArrayList;
import java.util.List;

/**
 * csv文件行转列数据
 */
public class DataToColumnsConversion {
    public static void main(String[] args) {
        List<String[]> dataRows = new ArrayList<>();
        dataRows.add(new String[]{"A", "B", "C"});
        dataRows.add(new String[]{"D", "E", "F"});
        dataRows.add(new String[]{"G", "H", "I"});

        List<ColumnData> columns = convertToColumns(dataRows);

        // 输出列数据
        for (ColumnData column : columns) {
            System.out.println(column);
        }
    }

    private static List<ColumnData> convertToColumns(List<String[]> dataRows) {

        int rows = dataRows.size();
        int columnsCount = dataRows.get(0).length;

        List<ColumnData> columnDataList = new ArrayList<>();

        for (int j = 0; j < columnsCount; j++) {
            List<String> column = new ArrayList<>();
            for (int i = 0; i < rows; i++) {
                String value = dataRows.get(i)[j];
                column.add(value);
            }
            ColumnData columnData = new ColumnData();
            columnData.setColumn(dataRows.get(0)[j]);
            columnData.setDatas(column);
            columnDataList.add(columnData);
        }

        return columnDataList;
    }
}