import com.cpic.blood.core.dto.ColumnData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CSVtoColumn {
    public static void main(String[] args) {
        String inputCsvFile = "D:\\code\\dmgr_sit_lf18gigms_illness_chwj_2023_05_31.csv";
        String outputCsvFile = "output.csv";
        String line;
        String delimiter = "\t";
        List<String[]> dataRows = new ArrayList<>();

        // 读取CSV数据行
        try (BufferedReader br = new BufferedReader(new FileReader(inputCsvFile))) {
            while ((line = br.readLine()) != null) {
                String[] row = line.split(delimiter);
                dataRows.add(row);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> firstFields = dataRows.stream()
                .map(row -> row[0])
                .collect(Collectors.toList());

        List<ColumnData> columnDataList = new ArrayList<>();
        List<String> list = Arrays.asList(dataRows.stream().findFirst().get());
        for (String s : list) {
            System.out.println(s);
        }

        System.out.println("转换完成并输出到文件：" + outputCsvFile);
    }
}