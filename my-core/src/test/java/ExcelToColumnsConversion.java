import com.cpic.blood.core.dto.ColumnData;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelToColumnsConversion {
    public static void main(String[] args) {
        String filePath = "D:\\mywork\\test\\dmgr_sit_lf18gigms_illness_chwj_2023_05_30.xlsx";

        try (FileInputStream fileInputStream = new FileInputStream(filePath);
             Workbook workbook = new XSSFWorkbook(fileInputStream)) {

            Sheet sheet = workbook.getSheetAt(0);

            List<ColumnData> columns = convertToColumns(sheet);

            // ���������
            for (ColumnData column : columns) {
                System.out.println(column);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<ColumnData> convertToColumns(Sheet sheet) {

        int rowCount = sheet.getPhysicalNumberOfRows();
        int columnCount = sheet.getRow(0).getPhysicalNumberOfCells();

        List<ColumnData> columnDataList = new ArrayList<>();

        for (int j = 0; j < columnCount; j++) {
            List<String> column = new ArrayList<>();
            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);
                Cell cell = row.getCell(j);
                String value = getStringValueFromCell(cell);
                column.add(value);
            }
            ColumnData columnData = new ColumnData();
            String value = getStringValueFromCell(sheet.getRow(0).getCell(j));
            columnData.setColumn(value);
            columnData.setDatas(column);
            columnDataList.add(columnData);
        }

        return columnDataList;
    }

    private static String getStringValueFromCell(Cell cell) {
        String value = "";
        if (cell != null) {
            CellType cellType = cell.getCellType();
            if (cellType == CellType.STRING) {
                value = cell.getStringCellValue();
            } else if (cellType == CellType.NUMERIC) {
                double numericValue = cell.getNumericCellValue();
                value = String.valueOf(numericValue);
            } else if (cellType == CellType.BOOLEAN) {
                boolean booleanValue = cell.getBooleanCellValue();
                value = String.valueOf(booleanValue);
            }
        }
        return value;
    }
}