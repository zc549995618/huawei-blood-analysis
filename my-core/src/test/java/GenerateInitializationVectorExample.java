import java.security.SecureRandom;
import java.util.Base64;

public class GenerateInitializationVectorExample {
    public static void main(String[] args) {
        // 生成一个随机的16字节（128位）初始化向量
        byte[] iv = generateRandomInitializationVector(16);

        // 将初始化向量转换为Base64编码字符串
        String base64IV = Base64.getEncoder().encodeToString(iv);

        System.out.println("Generated Initialization Vector: " + base64IV);
    }

    private static byte[] generateRandomInitializationVector(int size) {
        SecureRandom secureRandom = new SecureRandom();
        byte[] iv = new byte[size];
        secureRandom.nextBytes(iv);
        return iv;
    }
}