import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtAuthenticationTest {

    private static final String SECRET_KEY = "6Ej/Sipy7FYmmc9c6fjoosCAwdheuIOaRoiEJ1C4l74=";
    private static final long EXPIRATION_TIME = 86400000; // 24 hours

    private UserDetailsService userDetailsService;
    private PasswordEncoder passwordEncoder;

    public JwtAuthenticationTest(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        UserDetailsService userDetailsService = new CustomUserDetailsService();
        JwtAuthenticationTest jwtAuthenticationTest = new JwtAuthenticationTest(userDetailsService);

        // 创建一个用户
        UserDetails userDetails = jwtAuthenticationTest.createUser("admin", "123456", "admin");

        // 对用户进行身份验证并生成JWT令牌
        String token = jwtAuthenticationTest.generateToken(userDetails);

        System.out.println(token);

        // 使用JWT令牌进行身份验证
        UserDetails authenticatedUser = jwtAuthenticationTest.validateToken(token);

        System.out.println("Authenticated User: " + authenticatedUser.getUsername());
    }

    public UserDetails createUser(String username, String password, String... roles) {
        UserDetails userDetails = User.withUsername(username)
                .password(passwordEncoder.encode(password))
                .roles(roles)
                .build();

        // 将用户存储在UserDetailsService中
        ((CustomUserDetailsService) userDetailsService).addUser(userDetails);

        return userDetails;
    }

    public String generateToken(UserDetails userDetails) {
        Date now = new Date();
        Date expiration = new Date(now.getTime() + EXPIRATION_TIME);

        String token = Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(now)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();

        return token;
    }

    public UserDetails validateToken(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token)
                    .getBody();

            String username = claims.getSubject();

            if (username != null) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);

                Authentication authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());

                SecurityContextHolder.getContext().setAuthentication(authentication);

                return userDetails;
            }
        } catch (Exception e) {
            System.out.println("Invalid token: " + e.getMessage());
        }

        return null;
    }

    static class CustomUserDetailsService implements UserDetailsService {

        private Map<String, UserDetails> users = new HashMap<>();

        public void addUser(UserDetails userDetails) {
            users.put(userDetails.getUsername(), userDetails);
        }

        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            UserDetails userDetails = users.get(username);

            if (userDetails == null) {
                throw new UsernameNotFoundException("User not found");
            }

            return userDetails;
        }
    }
}