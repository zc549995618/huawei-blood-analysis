#!/bin/bash

# 设置日志文件路径
export LOG_FILE=./logs/

# 设置外部配置文件路径
export CONFIG_FILE=config

# 设置 JVM 调优参数
JAVA_OPTS="-server -Xms4g -Xmx8g -Djava.security.egd=file:/dev/./urandom  -Dfile.encoding=UTF-8"
JAVA_OPTS="$JAVA_OPTS -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=512m"
JAVA_OPTS="$JAVA_OPTS -XX:+UseG1GC -XX:ParallelGCThreads=8"
JAVA_OPTS="$JAVA_OPTS -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./logs/"

# 设置其他参数
APP_OPTS="-Dcas.standalone.config=$CONFIG_FILE -Dlog.level=INFO"

# 运行 Java 程序
nohup java $JAVA_OPTS $APP_OPTS -jar huawei-blood-analysis-1.0.jar &